#!/bin/bash
gitclonepull() {
        cdk_addon_type=${1}
        cdk_addon_repo_proto="https://"
        cdk_addon_repo=bitbucket.org/leonardo-consulting/lc-cdk-addon-${cdk_addon_type}.git
        cdk_addon_repo_url=${cdk_addon_repo_proto}${cdk_addon_repo}
        cdk_addon_dir=${MINISHIFT_HOME}/repo/$(basename ${cdk_addon_repo} | sed 's/.git//g')
        if [ -d "${cdk_addon_dir}/.git" ]; then
                #git_cmd=pull
                echo "[INFO]: GIT directory ${cdk_addon_dir}/.git already exists."
                while (
                case "${update_repo}" in
                        Y|y|N|n) false;;
                        *) true
                esac
                )
                do
                        read -p  "[INPUT]: Do you want to update the repository? [Y|y|N|n] :" update_repo
                done
                case "${update_repo}" in
                        Y|y) echo "[EXECUTING]: ( cd ${cdk_addon_dir}; git pull origin master)"
                                                ( cd ${cdk_addon_dir}; git pull origin master) 2>&1
                                ;;
                        *) :
                esac
                update_repo=""
        else
                git_cmd=clone
                echo "[EXECUTING]: mkdir -p ${cdk_addon_dir}"
                                   mkdir -p ${cdk_addon_dir}
                echo "[EXECUTING]: git ${git_cmd} ${cdk_addon_repo_url} ${cdk_addon_dir}"
                                   git ${git_cmd} ${cdk_addon_repo_url} ${cdk_addon_dir}
        fi
}

chkreturn() {
        [[ ${1} -ne 0 ]] && { echo -e "\t\t[ERROR] encountered..exiting"; exit ; }
}

vm_config_input() {

        echo -e "\n[Please enter the required information for your minishift VM configuration]"
###=== Request no. of cpu as input ===###
        while (
        case "${cpu}" in
                1|2|4) false ;;
                *) true
        esac
        )
        do
                echo
                read -p "Minishift CPUs to be configured [1|2|4]: " cpu
        done

###=== Request size of RAM as input ===###
        while (
        case "${ram}" in
                8|10|12|16) false ;;
                *) true
        esac
        )
        do
                read -p "Minishift RAM (GB) to be configured [6|8|10|12|16]: " ram
        done

###=== Request type of hypervisor as input ===###
        #while (
        #case "${hypervisor}" in
                #native|virtualbox) false ;;
                #*) true
        #esac
        #)
        #do
                #read -p "Type of hypervisor to be configured [native|virtualbox]: " hypervisor
        #done
        # Harcoding as only allowing virtualbox as cross-platform hypervisor.
        hypervisor=virtualbox

###=== Request type of add-ons to install ===###
        while (
        case "${addontype}" in
                demo-ccd|dev-sandbox|pie-mortgage) false ;;
                *) true
        esac
        )
        do
                read -p "Your PIE and PAMlet to be configured [demo-ccd|dev-sandbox|pie-mortgage]: " addontype
        done

        case "${addontype}" in
		pie-*) while (
        		case "${baseenv}" in
                		dev-sandbox) false ;;
                		*) true
			esac 
			)
        		do
        			case "${addontype}" in
					pie-*) read -p "Base project for \"${addontype}\" [dev-sandbox]: " baseenv
				esac
			done
	esac

###=== Request pie install or uninstall ===###
        while ( 
	case "${addon_installremove}" in
		install|remove|scaledown) false;;
		*) true
	esac
	)
        do
                read -p "Install or Remove ${addontype} [install|remove|scaledown]: " addon_installremove
        done

###=== Request redhat developer username ===###
	if [ "${addon_installremove}" == "install" ]; then
        	while [ -z "${username}" ]
        	do
                	read -p "Your redhat developer username [E.g: bob.brown@abc.com]: " username
        	done

###=== Request redhat developer password ===###
        	while [ -z "${password}" ]
        	do
                	read -sp "Your redhat developer password: " password
                	#password=$(systemd-ask-password Your redhat developer password:)
        	done
        	echo
	fi

###=== Request start Minishift ===###
        #while (
        #case "${start_vm}" in
                #yes|no) false ;;
                #*) true
        #esac
        #)
        #do
                #read -p "Start minishift cdk post configuration? [yes|no]: " start_vm
        #done

###=== Request confirmation to proceed ===###
        while [ "${proceed}" != "yes" ]
        do
                echo
                read -p "Hello \"${username}\". You've requested for a minishift VM of the following attributes:
   VM vCPUs:                 $cpu
   VM RAM Size[GB]:          $ram
   VM Hypervisor Type:       $hypervisor
   VM Minishift Addons:      $addontype
   VM Minishift Addon Task:  $addon_installremove
   VM Minishift BaseEnv:     $baseenv
Type 'Y|y' to proceed, 'R|r' to re-enter details and C|c' to Cancel: " userinput
   #VM Minishift start:       ${start_vm}
                case "${userinput}" in
                        Y|y) proceed=yes ;;
                        R|r) cpu=""; ram=""; username=""; password=""; addontype=""; addon_installremove=""; start_vm=""; baseenv="" ; vm_config_input;;
                        C|c) echo "Exiting script."; exit;;
                        *) echo "Invalid response."
                esac
        done
}

addoninstallenable() {
	for addoninfo in ${1}
	do
        	addon=$(basename ${addoninfo} | awk -F: '{print $1}')
        	addonpri=$(basename ${addoninfo} | awk -F: '{print $2}')
        	addoninstallremove=$(basename ${addoninfo} | awk -F: '{print $3}')

		case "${addoninstallremove}" in
		   	install) echo -e "\n[EXECUTING]  ( cd ${cdk_addon_dir}/; ${MINISHIFT_BIN} addons ${addoninstallremove} -f ${addon})"
                               	                         ( cd ${cdk_addon_dir}/; ${MINISHIFT_BIN} addons ${addoninstallremove} -f ${addon} 2>&1 )
                               	       			 chkreturn ${?}
							 sleep 5
				 echo -e "\n[EXECUTING]  ${MINISHIFT_BIN} addons enable ${addon} --priority=${addonpri}"
							 ${MINISHIFT_BIN} addons enable ${addon} --priority=${addonpri} 2>&1
							 chkreturn ${?}
							 sleep 2
        			 echo -e "\n[EXECUTING]  ${MINISHIFT_BIN} addons apply ${addon}"
							 ${MINISHIFT_BIN} addons apply ${addon} 2>&1
							 chkreturn ${?}
				;;
		   	remove) echo -e "\n[EXECUTING]   ( cd ${cdk_addon_dir}/; ${MINISHIFT_BIN} addons ${addoninstallremove} ${addon})"
                               	                         ( cd ${cdk_addon_dir}/; ${MINISHIFT_BIN} addons ${addoninstallremove} ${addon} 2>&1 )
                               	       			 chkreturn ${?}
				;;
			scaledown) echo -e "\n[INFO] No addon tasks required for scaledown for ${profile_name}.\n"
		esac
	done
}

deployment_stagger() {
	sleep_secs=15
	project_name=${1}
	dc_add_on_type=${2}
        replicanum=${3}

	case "${dc_add_on_type}" in
        	#demo-ccd) DC="fsi-ccd-bp-postgresql fsi-ccd-bp-kieserver fsi-ccd-ux-admin-engine fsi-ccd-ux-customer-engine" ;;
        	demo-ccd) DC="demo-ccd-rhpam74-kieserver demo-ccd-entando-customer-engine demo-ccd-entando-backoffice-engine" ;;
        	demo-wwc) DC="" ;;
        	dev-sandbox) DC="pie-rhpam74-rhpamcentr pie-rhpam74-kieserver" ;;
        	pie-mortgage) DC="mortgage-entando-applicant-engine mortgage-entando-lender-engine pie-mortgage-curl" ;;
        	dev) DC="" ;;
        	*) echo -e "[ERROR]: no such addons - ${add_on_type}"; exit
	esac

	echo -e "\n[START_DEPLOYMENT_STAGGER]: $(date)"
	for dc in ${DC}
	do
        	echo -e "\nScaling replica of DC: ${dc}"
        	echo -e "[EXECUTING]: ${OC_BIN} scale deploymentconfig ${dc} -n ${project_name} --replicas=${replicanum}"
                              	${OC_BIN} scale deploymentconfig ${dc} -n ${project_name} --replicas=${replicanum} | awk '{print "\t"$0}'
                                sleep 5
        	cont="false"
        	while [ true ]
        	do
                	dc_rc=$(${OC_BIN} get deploymentconfig ${dc} --no-headers -n ${project_name} | awk '{print $1"-"$2}')
                	desired_current_ready=$(${OC_BIN} get replicationcontroller ${dc_rc} --no-headers -n ${project_name} | awk '{print $2":"$3":"$4}')
                	desired=$(echo ${desired_current_ready} | awk -F: '{print $1}')
                	current=$(echo ${desired_current_ready} | awk -F: '{print $2}')
                	ready=$(echo ${desired_current_ready} | awk -F: '{print $3}')
                	if [ "${current}" == "${desired}" -a "${desired}" == "${ready}" ]; then
                        	echo -e "\t[${dc_rc}][$(date)] Replica count MATCH: DESIRED=${desired} CURRENT=${current} READY=${ready}. Next.."
                        	break
                	else
                        	echo -e "\t[${dc_rc}][$(date)] Replica count not matching: DESIRED=${desired} CURRENT=${current} READY=${ready} .Sleeping for ${sleep_secs}..."
                        	sleep ${sleep_secs}
                	fi
        	done
		#if [ "${dc}" == "pie-mortgage-curl" ]; then
			 #echo -e "[EXECUTING]: ${OC_BIN} scale deploymentconfig ${dc} -n ${project_name} --replicas=0"
                                		#${OC_BIN} scale deploymentconfig ${dc} -n ${project_name} --replicas=0 | awk '{print "\t"$0}'
                                		#sleep 5
		#fi
	done
	echo -e "\n[END_DEPLOYMENT_STAGGER]: $(date)"
	echo
}

stopcdkvm() {
        stopprofilename=${1}
        stopprofileactive=${2}

        echo "[INFO] You have a running VM of [${stopprofilename}]. Stop the VM to conserve resources."
        while (
        case "${stopvm}" in
                Y|y|N|n) false ;;
                *) true
        esac
        )
        do
                echo -n "Stop the current runing VM with profile [${stopprofilename}]: [Y|y|N|n]: "
                read stopvm </dev/tty
        done
        case "${stopvm}" in
                Y|y) echo -e "[EXECUTING] ${MINISHIFT_BIN} profile set ${stopprofilename}" ;
                                          ${MINISHIFT_BIN} profile set ${stopprofilename} 2>&1
                               		  chkreturn ${?}

                        echo -e "[EXECUTING] ${MINISHIFT_BIN} stop" ;
                                             ${MINISHIFT_BIN} stop 2>&1
                               		     chkreturn ${?}
        esac
        stopvm=""
}

checkrunningvm() {
        IFS=
        cdkprofiles_info=$(${MINISHIFT_BIN} profile list | sed 's/- //g' | tr -s "\t" | awk -F"\t" '{print "name="$1"|vmstate="$2"|profileactive="$3}')
        runningvms=$(echo ${cdkprofiles_info} | grep "vmstate=Running" )
        echo ${runningvms} | while read running_vm
        do
                if [ ! -z "${running_vm}" ]; then
                        running_profilename=$(echo ${running_vm} | awk -F\| '{print $1}' | awk -F= '{print $2}')
                        running_profileactive=$(echo ${running_vm} | awk -F\| '{print $3}' | awk -F= '{print $2}')
			if [ "${profile_name}" != "${running_profilename}" ]; then
                        	stopcdkvm ${running_profilename} ${running_profileactive}
			fi
                fi
        done
        IFS=" "
}

vm_existing_profile() {
        IFS=
        cdkprofiles_info=$(${MINISHIFT_BIN} profile list | sed 's/- //g' | tr -s "\t" | awk -F"\t" '{print "name="$1"|vmstate="$2"|profileactive="$3}')
        #runningvms=$(echo ${cdkprofiles_info} | grep "vmstate=Running" )
        echo ${cdkprofiles_info} | while read cdkprofile
        do
                if [ ! -z "${cdkprofile}" ]; then
                        existing_profilename=$(echo ${cdkprofile} | awk -F\| '{print $1}' | awk -F= '{print $2}')
                        existing_profileactive=$(echo ${cdkprofile} | awk -F\| '{print $3}' | awk -F= '{print $2}')
                        existing_profile_state=$(echo ${cdkprofile} | awk -F\| '{print $2}' | awk -F= '{print $2}' | tr " " "_")
                        echo ${existing_profilename}:${existing_profileactive}:${existing_profile_state}
                fi
        done
        IFS=" "

}

download_cdk() {
	echo -e "[EXECUTING]: curl -w '%{http_code}' ${silent} ${cdklink} -o ${cdk_srcbinfile} "
	curl_http_rc=$(curl -w '%{http_code}' ${silent} ${cdklink} -o ${cdk_srcbinfile} )
	curl_rc=${?}
	case "${curl_http_rc}" in
        	200) : ;;
        	*) rm ${cdk_srcbinfile} ; echo -e "\n[ERROR] Curl http error = ${curl_http_rc} .Exiting...\n" && exit 1
	esac
}

set_oc_bin() {
	OC_BIN=$(find ${MINISHIFT_HOME} -type f -name "oc${fileext}")
	if [ ! -z "${OC_BIN}" ]; then
		echo -e "\n[INFO] OC_BIN=${OC_BIN}\n"
	else
		echo -e "\n[ERROR]: OC binary not found. Exiting." ; exit 1
	fi
}

#=================================================================####=================================================================#
#=================================================================MAIN=================================================================#
#=================================================================####=================================================================#

# Using binaries downloaded and placed into LC S3
cdk390maclink=https://lss-cdk.s3-ap-southeast-2.amazonaws.com/cdk-3.9.0-1-minishift-darwin-amd64
cdk390winlink=https://lss-cdk.s3-ap-southeast-2.amazonaws.com/cdk-3.9.0-1-minishift-windows-amd64.exe
cdk390linlink=https://lss-cdk.s3-ap-southeast-2.amazonaws.com/cdk-3.9.0-1-minishift-linux-amd64

if [ ! -z "${1}" ]; then
	scriptarg=${1}
fi

case ${OSTYPE} in
	darwin*) cdklink=${cdk390maclink} ;;
	linux-gnu) WSL=$(mount -v | grep "/mnt/c")
		   if [ ! -z "${WSL}" ]; then
			cdklink=${cdk390winlink}
			fileext=.exe
		   else
			cdklink=${cdk390linlink}
		   fi
			 ;;
	cygwin|msys|win32) cdklink=${cdk390winlink} ;;
	*) echo -e "\n[ERROR]: ${OSTYPE} not supported for cdk." && exit
esac

cdk390homedir=~/cdk390
cdkminishift_homedir=${cdk390homedir}/minishift
cdk_srcbinfile=${cdk390homedir}/bin/$(basename ${cdklink})
cdk_binfile=${cdk390homedir}/bin/minishift${fileext}

if [ ! -d ${cdk390homedir}/bin ]; then
	echo -e "\n[EXECUTING]: mkdir -p ${cdk390homedir}/bin"
                        	mkdir -p ${cdk390homedir}/bin
fi

if [ ! -f ${cdk_srcbinfile} ]; then
	download_cdk
else
	echo -e "\n[INFO]: cdk file ${cdk_srcbinfile} already exists."
fi

if [ ! -f  ${cdk_srcbinfile} ]; then
	echo -e "\n[ERROR]: cdk download of ${cdklink} may have failed." && exit
fi

if [ ! -f ${cdk_binfile} -a -f ${cdk_srcbinfile} ]; then
	echo -e "\n[EXECUTING]: cp ${cdk_srcbinfile} ${cdk_binfile} && chmod +x ${cdk_binfile}"
                        	cp ${cdk_srcbinfile} ${cdk_binfile} && chmod +x ${cdk_binfile}
else
	echo -e "\n[INFO]: cdk file ${cdk_binfile} copy of ${cdk_srcbinfile} already exists."
fi

GREPPATH=$(echo "${PATH}" | grep ${cdk390homedir}/bin)
if [ ! -z "${GREPPATH}" ]; then
	:
else
	echo -e "\n[EXECUTING]: export PATH=$(dirname ${cdk_binfile}):${PATH}"
	                        export PATH=$(dirname ${cdk_binfile}):${PATH}
fi

echo -e "\n[INFO] minishift binary is $(which minishift${fileext})"	

dirempty=$(ls -A "${cdkminishift_homedir}" 2>/dev/null)
if [ -z "${dirempty}" -o "${scriptarg}" == "-fc" ]; then
	echo
        echo "[EXECUTING]: export MINISHIFT_HOME=${cdkminishift_homedir}"
                           export MINISHIFT_HOME=${cdkminishift_homedir}
	echo
	echo "[EXECUTING]: ${cdk_binfile} setup-cdk --minishift-home ${cdkminishift_homedir}"
         	 	   ${cdk_binfile} setup-cdk --minishift-home ${cdkminishift_homedir}
else
	echo
	echo "[INFO]: CDK homedir \"${cdkminishift_homedir}\" already exists. To re-create contents within \"${cdkminishift_homedir}\", please pass '-fc' as argument."
fi

echo
echo "[EXECUTING]: export MINISHIFT_HOME=\"${cdkminishift_homedir}\""
	           export MINISHIFT_HOME="${cdkminishift_homedir}"

echo "[INFO] MINISHIFT_HOME=${MINISHIFT_HOME}"

BIN_EXE=$(env | grep -w WIN10DIR)
if [ -z "${BIN_EXE}" ]; then
        MINISHIFT_BIN=$(which minishift)
else
        MINISHIFT_BIN=$(which minishift.exe)
fi

if [ -z "${MINISHIFT_BIN}" ]; then
        echo "[ERROR] Please export your minishift path. Exiting."
        exit
fi

echo -e "\n[INFO] Existing profiles and state: "
vm_existing_profile | awk '{print "\t"$0}'

vm_config_input

case "${baseenv}" in
        dev-sandbox) profile_name="dev-sandbox-profile-${cpu}vcpu-${ram}gb" ;
			add_on_list="dev-sandbox ${addontype}"
			echo "####===add_on_list[ ${add_on_list} ]===####"
			;;
        demo-ccd) profile_name="demo-ccd-profile-${cpu}vcpu-${ram}gb" ;
			add_on_list="demo-ccd ${addontype}"
			echo "####===add_on_list[ ${add_on_list} ]===####"
			;;
        *) profile_name="${addontype}-profile-${cpu}vcpu-${ram}gb"
			add_on_list="${addontype}"
esac

checkrunningvm

profile_exists=$(vm_existing_profile | grep ${profile_name})
if [ -z "${profile_exists}" ]; then
	if [ "${addon_installremove}" == "install" ]; then
		echo -e "\n[EXECUTING] ${MINISHIFT_BIN} profile set ${profile_name}"
                               	       ${MINISHIFT_BIN} profile set ${profile_name} 2>&1
                               	       chkreturn ${?}

		echo -e "\n[EXECUTING] ${MINISHIFT_BIN} --profile ${profile_name} config set memory ${ram}GB"
                       	       	       ${MINISHIFT_BIN} --profile ${profile_name} config set memory ${ram}GB 2>&1
                               	       chkreturn ${?}
	
		echo -e "\n[EXECUTING] ${MINISHIFT_BIN} --profile ${profile_name} config set cpus ${cpu}"
                       	       	       ${MINISHIFT_BIN} --profile ${profile_name} config set cpus ${cpu} 2>&1
                               	       chkreturn ${?}

		case "${hypervisor}" in
        		virtualbox) echo -e "\n[EXECUTING] ${MINISHIFT_BIN} --profile ${profile_name} config set vm-driver virtualbox" ;
                                           	   	   ${MINISHIFT_BIN} --profile ${profile_name} config set vm-driver virtualbox 2>&1 ;
                                                   	   chkreturn ${?}
					;;
        		#hyperv)     echo -e "\n[EXECUTING] ${MINISHIFT_BIN} --profile ${profile_name} config set hyperv-virtual-switch ext_vswitch" ;
                                           	   	   #${MINISHIFT_BIN} --profile ${profile_name} config set hyperv-virtual-switch ext_vswitch 2>&1 ;
                                                   	   #chkreturn ${?}
		esac
	else
		echo -e "\n[ERROR] There is no existing profile [ ${profile_name} ] for [ ${addon_installremove} ] action.\n"
                echo -e "\n[INFO] Profiles and state: "
                vm_existing_profile | awk '{print "\t"$0}'
		echo -e "\n[ERROR]: Exiting.\n" && exit 1
	fi
else
	echo -e "\n[EXECUTING] ${MINISHIFT_BIN} profile set ${profile_name}"
                               ${MINISHIFT_BIN} profile set ${profile_name} 2>&1
                               chkreturn ${?}
fi

for addon_info in admin-user:enable xpaas:disable anyuid:disable
do
        addon_name=${addon_info%%:*}
        addon_state=${addon_info##*:}
        echo -e "\n[EXECUTING] ${MINISHIFT_BIN} addons ${addon_state} ${addon_name}"
                               ${MINISHIFT_BIN} addons ${addon_state} ${addon_name} 2>&1
                               chkreturn ${?}
done

for add_on in ${add_on_list}
do
	echo -e "\n[INFO] Processing add_on  [ ${add_on} ]"	
	case "${add_on}" in
        	dev-sandbox) ADDONS="dev-sandbox:5"; PROJECT_NAME=${add_on}; gitclonepull ${add_on}; addoninstallenable ${ADDONS}:${addon_installremove};;
        	pie-mortgage) ADDONS="pie-mortgage:6"; PROJECT_NAME=${add_on}; gitclonepull ${add_on}; addoninstallenable ${ADDONS}:${addon_installremove};;
        	demo-wwc) ADDONS="demo-wwc:5"; PROJECT_NAME=${add_on}; gitclonepull ${add_on}; addoninstallenable ${ADDONS}:${addon_installremove};;
        	demo-ccd) ADDONS="demo-ccd:5"; PROJECT_NAME=${add_on}; gitclonepull ${add_on}; addoninstallenable ${ADDONS}:${addon_installremove}
	esac
	sleep 5
done

# This variable is temporary as we will be looking to start the VM
start_vm=yes
case "${start_vm}" in
        yes) set_oc_bin
	     if [ "${addon_installremove}" == "install" ]; then
		if [ "${showpassword}" = "true" ]; then
               		 # Maybe good to add --ocp-tag=v3.11.104, valid only for cdk
                	echo -e "\n[EXECUTING] ${MINISHIFT_BIN} start --profile ${profile_name} --username ${username} --password ${password}"
             	else
                	echo -e "\n[EXECUTING] ${MINISHIFT_BIN} start --profile ${profile_name} --username ${username} --password ***********"
             	fi
		echo "[START_VM_TIME]: $(date)"
		${MINISHIFT_BIN} start --profile ${profile_name} --username ${username} --password ${password} 2>&1
		chkreturn ${?}
		echo "[START_COMPLETE]: $(date)"
		echo -e "\nLogging in as admin.."
		echo -e "\n[EXECUTING] ${OC_BIN} login https://$(${MINISHIFT_BIN} ip):8443 -u admin -p password -n ${PROJECT_NAME}"
                     		       ${OC_BIN} login https://$(${MINISHIFT_BIN} ip):8443 -u admin -p password -n ${PROJECT_NAME} | awk '{print "\t"$0}'

		echo -e "[INFO] Starting minishift console"
		${MINISHIFT_BIN} console
             fi
		for add_on in ${add_on_list}
		do
			case "${addon_installremove}" in
				install) deployment_stagger ${add_on} ${add_on} 1
						;;
				scaledown) profile_stoppedq=$(vm_existing_profile | grep ${profile_name} | awk -F: '{print $3}')
					   case "${profile_stoppedq}" in
						Does_Not_Exist|Stopped) echo -e "\n[INFO]: ${profile_name} is \"${profile_stoppedq}\".\n" ;;
						*) deployment_stagger ${add_on} ${add_on} 0 ;;
					   esac
						;;
				*) :
			esac
		done
		if [ "${addon_installremove}" == "scaledown" ]; then
			echo -e "\n[EXECUTING] ${MINISHIFT_BIN} stop"
			                       ${MINISHIFT_BIN} stop 2>&1
			while(
        		case "${rm_vm_profile}" in
                		Y|y|N|n) false ;;
                		*) true
        		esac
			)
			do
				echo 
				read -p "[INPUT] REMOVE|DELETE current VM and profile \"${profile_name}\" [Y|y|N|n]: " rm_vm_profile
			done
			if [ "${rm_vm_profile}" == "Y" -o "${rm_vm_profile}" == "y" ]; then
				echo -e "\n[EXECUTING] ${MINISHIFT_BIN} delete --clear-cache"
				                       ${MINISHIFT_BIN} delete --clear-cache 2>&1
				echo -e "\n[EXECUTING] ${MINISHIFT_BIN} profile delete -f ${profile_name}"
				                       ${MINISHIFT_BIN} profile delete -f ${profile_name} 2>&1
			fi
			echo -e "\n[INFO] Profiles and state: "
			vm_existing_profile | awk '{print "\t"$0}'
			echo
		fi
             ;;
        *)  echo -e "\n[INFO] Minishift ${profile_name} will NOT be started up."
esac

